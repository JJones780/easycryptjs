EasyCryptJS
===========

Single Html file, run locally with no dependencies to encrypt/decrypt text easily.
 
Has verify features:
- button which displays command line you can use to verify the decryption using OpenSSL.
- View source button - encourage people to look at the source code.
- clearly partitioned code to let you re-download and drop in the encryption library: 
  ( https://github.com/neodon/gibberish-aes ). 

Use at your own risk. 

NOTE: Some web email services add in extra spaces in the middle of lines which will break your encoded block. 
Easycryptjs removes spaces before decrypting.
